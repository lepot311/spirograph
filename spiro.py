import Spirograph
from Spirograph.renderers.svgrenderer import SVGRenderer
from Spirograph.palettes import ColourLoversPalette


Spirograph.main(renderer=SVGRenderer(), palette=ColourLoversPalette())
