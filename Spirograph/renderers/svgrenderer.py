import os
import shutil
import sys

import cairo


class SVGRenderer(object):
    def __init__(self):
        self.color = None
        self.previous = None
        self.current = 0.0, 0.0
        self.W = 500
        self.H = 500

        self.surface = cairo.SVGSurface('svg/.temp.svg', self.W, self.H)
        self.ctx = cairo.Context(self.surface)
        self.ctx.set_line_width(1)
        self.ctx.set_line_join(cairo.LINE_JOIN_ROUND)  # not sure if working
        self.ctx.translate(self.W / 2.0, self.H / 2.0)

        self.move_to(*self.current)

    def move_to(self, x, y):
        self.current = x, y
        self.ctx.move_to(*self.current)

    def line_to(self, x, y):
        self.previous = self.current
        self.current = x, y
        self.ctx.line_to(*self.current)

    def set_color(self, color):
        self.ctx.set_source_rgb(*color)
        self.ctx.stroke()
        return

    def save(self, name):
        # If the output directory doesn't exist, create it.
        try:
            os.mkdir('svg')
        except OSError:
            pass
        # The way Cairo does Surfaces is a bit odd. We have to declare the
        # filename at the beginning, before we know the Palette. I may change
        # this later by passing in the palette during Renderer initialization.
        # For now we just init with a temp file and move it when save is called.
        shutil.move('svg/.temp.svg', "svg/{}.svg".format(name))

    def get_canvas(self):
        return self.surface

    def clear(self):
        pass

    @property
    def width(self):
        return self.W

    @property
    def height(self):
        return self.H
