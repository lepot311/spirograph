from turtle import Turtle


class TurtleRenderer(object):
    def __init__(self, t=None):
        self.t = t or Turtle()
        self.t.hideturtle()

    def move_to(self, x, y):
        self.t.up()
        self.t.setpos(x, y)

    def line_to(self, x, y):
        self.t.down()
        self.t.setpos(x, y)

    def set_color(self, color):
        self.color = color
        self.t.color(*self.color)

    def save(self, file=None):
        print(file)
        self.get_canvas().postscript(file=file)

    def get_canvas(self):
        return self.t.getscreen().getcanvas()

    def draw_text(self, text, align=None):
        M = min(self.width, self.height) // 2  # TODO: REPEATS
        # write palette name
        self.move_to(0, M-40)
        self.t.down()
        try:
            self.t.write(text, align=align)
        except UnicodeEncodeError:
            return
        self.t.up()

    def clear(self):
        self.t.clear()

    @property
    def width(self):
        return self.get_canvas().winfo_width()

    @property
    def height(self):
        return self.get_canvas().winfo_height()
