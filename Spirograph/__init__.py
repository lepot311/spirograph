from fractions import gcd
from functools import partial
from math import radians
from random import randint, uniform
from time import sleep

from Spirograph import shapes
from Spirograph.renderers.turtlerenderer import TurtleRenderer
from Spirograph.palettes import RandomPalette


def hex_to_float(hexstr):
    _hex = hexstr.strip('#')
    r, g, b = int(_hex[:2], 16), int(_hex[2:4], 16), int(_hex[4:], 16)
    r, g, b = r / 255.0, g / 255.0, b / 255.0
    return (r, g, b)


class Spirograph(object):
    def __init__(self, renderer=None, color=(0.0, 0.0, 0.0), R=300, r=50, l=0.9):
        self.function = partial(shapes.spirograph, R=R, r=r, l=l, k=float(r) / float(R))
        self.nRot = r // gcd(r, R)
        self.step = 5

        self.color = color
        self.renderer = renderer

    def draw(self):
        self.renderer.set_color(self.color)

        # move cursor to initial draw point
        x, y = self.function(0)
        self.renderer.move_to(x, y)

        # sweep across 360 degrees
        for i in range(0, 360 * self.nRot + 1, self.step):
            x, y = self.function(a=radians(i))
            self.renderer.line_to(x, y)


class SpirographGroup(object):
    def __init__(self, renderer=None, palette=None):
        self.renderer = renderer
        self.palette = palette
        self.colors = None

        self.size = min(self.renderer.width, self.renderer.height) // 2

        # TODO: kind of nasty
        try:
            self.palette.colors[0].startswith('#')
            self.colors = map(hex_to_float, self.palette.colors) 
        except:
            self.colors = self.palette.colors

        # generate spirographs
        self.spirographs = [ self.get_random_spirograph(color) \
                             for color in self.colors]

    def get_random_spirograph(self, color):
        M = self.size
        l = uniform(0.4, 0.9)
        R = 1
        r = 1
        nRot = 0

        # some reasonable limits
        while not nRot or nRot < 4 or nRot > 14 or r / R > 0.2:
            R = randint(M // 4, M)
            r = randint((M // 2) // 4, M // 2)
            nRot = r // gcd(r, R)

        return Spirograph(renderer=self.renderer, color=color, R=R, r=r, l=l)

    def draw(self):
        # print and draw title
        print(self.palette.title)
        #self.renderer.draw_text(self.palette.title, align="center")

        # draw each spiro
        [ s.draw() for s in self.spirographs ]

    def save(self):
        # save as eps
        self.renderer.save(self.palette.title)


def main(renderer=None, palette=None):
    renderer = renderer or TurtleRenderer()
    palette = palette or RandomPalette()
    while True:
        sg = SpirographGroup(
            renderer=renderer,
            palette=palette,
        )
        sg.draw()
        sg.save()
        #sleep(10)
        sg.renderer.clear()
        break

    #turtle.mainloop()

if __name__ == '__main__':
    main()
