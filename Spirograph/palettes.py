class RandomPalette(object):
    def __init__(self):
        from random import random

        self.title = "RANDOM"
        self.colors = [
            (random(), random(), random()) \
            for i in range(5)
        ]


class ColourLoversPalette(object):
    def __init__(self, name=None):
        from colourlovers import ColourLovers

        cl = ColourLovers()

        try:
            name = name or 'random'
            print("Fetching cl palette: %s" % name)
            self.palette = cl.palettes(name)[0]
        except:
            raise Exception('ColourLovers: Network time out')

    @property
    def colors(self):
        # Look at this horrendous spelling!
        return self.palette.colours

    @property
    def title(self):
        return self.palette.title
