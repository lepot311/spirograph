from math import cos, sin

# NOTE: these require angle a to be passed in as radians

def spirograph(a=0, R=300, r=50, l=0.9, k=1):
    x = int(R) * ((1-k) * cos(a) + l * k * cos((1-k) * a / k ))
    y = int(R) * ((1-k) * sin(a) + l * k * sin((1-k) * a / k ))
    return x, y


def circle(x, y, a):
    return x + r*math.cos(a), y + r*math.sin(a)
