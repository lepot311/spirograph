# Spirograph.py

Makes pretty pictures with Python's built-in Turtle module. Grabs interesting
color palettes from www.colourlovers.com. Saves the drawing to .svg format.

## Installation (OS X)
```
#!bash

brew install python3 py3cairo
python3 -m venv --system-site-packages env
. env/bin/activate
pip install -r Spirograph/requirements.pip
mkdir svg
python spiro.py
```

## Usage
There are two renderers included:

- Turtle
- SVG

Turtle is the default renderer, which will let you watch spirographs being
drawn in real-time. If you want to generate svg images instead, pass
SVGRenderer to the main function, like so:

```
Spirograph.main(renderer=SVGRenderer(), palette=ColourLoversPalette())
```
